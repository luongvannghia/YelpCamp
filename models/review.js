const mongoose = require('mongoose');
const Joi = require('joi');

// mongoose --------------------------------------------------------------------------------------------- START
const ReviewSchema_Mongoose = new mongoose.Schema({
    text    : String,
    rating  : Number,
    camp: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Campground'
    }// camp
}); // schema
// mongoose --------------------------------------------------------------------------------------------- END

// Joi -------------------------------------------------------------------------------------------------- START
// for error-handling with Joi
const Review_Empty = {
    text    : '',
    rating  : 1.0,
    camp    : '',
    error   : ''
}

// Joi schema
const ReviewSchema_Joi = Joi.object({
    text    : Joi.string().required(),
    rating  : Joi.number().min(1).max(10).required(),
    camp    : Joi.string().required()
}); // reviewSchema_Joi
// Joi -------------------------------------------------------------------------------------------------- START

module.exports = {
    Review: mongoose.model('Review', ReviewSchema_Mongoose),
    ReviewPrototype: Review_Empty,
    ReviewValidator: ReviewSchema_Joi
} // module.exports