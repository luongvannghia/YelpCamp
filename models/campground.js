const mongoose = require('mongoose');
const { Review } = require('./review');
const Joi = require('joi');

// mongoose --------------------------------------------------------------------------------------------- START
CampgroundSchema_Mongoose = new mongoose.Schema({
    title: String,
    image: {
        type: String,
        default: 'https://source.unsplash.com/collection/8613850'
    }, // image
    price: {
        type: Number,
        default: 1.0
    }, // price
    description: {
        type: String,
        default: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi quaerat aperiam provident nam odit quasi! Nihil hic iure, iusto ea ut voluptates consequuntur molestias eos, a, autem tempore eligendi assumenda.'
    }, // description
    location: String,
    reviews: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Review'
    }]// reviews
}); // schema

// middleware to remove all reviews of a camp when deleted
CampgroundSchema_Mongoose.post('findOneAndDelete', async function(camp) {
    if (camp.reviews.length)
        await Review.deleteMany({ _id: { $in: camp.reviews } });
});
// mongoose --------------------------------------------------------------------------------------------- END

// Joi -------------------------------------------------------------------------------------------------- START
// for error-handling with Joi
const Campground_Empty = {
    title       : '',
    price       : 1.0,
    description : '',
    location    : '',
    image       : 'https://source.unsplash.com/collection/8613850',
    error       : ''
}

// Joi schema
const CampgroundSchema_Joi = Joi.object({
    title       : Joi.string().required(),
    price       : Joi.number().min(0.1).required(),
    description : Joi.string().allow(null, ''),
    location    : Joi.string().allow(null, ''),
    image       : Joi.string().allow(null, '')
}); // CampgroundSchema_Joi
// Joi -------------------------------------------------------------------------------------------------- START

module.exports = {
    Campground: mongoose.model('Camp', CampgroundSchema_Mongoose),
    CampgroundPrototype: Campground_Empty,
    CampgroundValidator: CampgroundSchema_Joi
} // module.exports