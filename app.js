const express = require('express');
const path = require('path');
const colorman = require('colors');
const mongoose = require('mongoose');
const { Campground, CampgroundPrototype, CampgroundValidator } = require('./models/campground'); // CampgroundValidator with Joi
const Quotes = require('./data/adventure-quotes.json')

const CONST_INT_APP_PORT = 80;
const CONST_CONNECTION_STRING = 'mongodb://localhost:27017/yelpcamp';
const CONST_CONNECTION_OPTIONS = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
}; // CONST_CONNECTION_OPTIONS

// app -------------------------------------------------------------------------------------------------- START
const app = express();

// app.set ------------------------------------------------ START
app.set('view engine', 'ejs');
app.set( 'views', path.join(__dirname, 'views') );
// app.set ------------------------------------------------ END

// middleware --------------------------------------------- START
app.use( express.static( path.join(__dirname, '/assets') ) ); // for serving static assets (images, styles, js files...)
app.use( express.json() );
app.use( express.urlencoded({ extended: true }) );

// general middleware to add a timestamp for each request
app.use( (req, res, next) => {
    req.requestTime = Date.now();

    next();
}); // app.use

// used to protect a specific site
app.use('/protected-site', (req, res, next) => {
    const { password } = req.query;

    if (password == 'dolalala.me') {
        res.send('Just a implementation of middleware.');
    } // if
    else
        res.status(401).send('Password required to access this page.'); // 401 = unauthorized    
}); // app.use

// middleware as a function
// used to protect a specific site
const verifyPassword = (req, res, next) => {
    const { password } = req.query;

    if (password == 'dolalala.me') {
        next();
    } // if
    else
        res.status(401).send('Password required to access this page.'); // 401 = unauthorized
}; // verifyPassword
// middleware --------------------------------------------- START

// start yelpcamp
app.listen(CONST_INT_APP_PORT, () => {
    console.log( colorman.magenta(`express/> YelpCamp running on port ${CONST_INT_APP_PORT}.`) );
}); // app.listen
// app --------------------------------------------------------------------------------------------------  END

// ROUTING ----------------------------------------------------------------------------------------------- START
// executed in order -------------------------------------------------------------------------------------------
app.get('/', (req, res) => {
    const quotesCount = Quotes.length;
    const randomQuoteNumber = Math.ceil(Math.random() * quotesCount);
    const quote = Quotes[randomQuoteNumber];

    res.render('home', { quote });
}); // app.get '/'

app.get('/camps', async (req, res) => {
    const camps = await Campground.find({});
    res.render('camps/index', { camps});
}); // get '/camps'

/* camps/new --------------------------------------------------- START */
// this route must be BEFORE the route app.get('/camps/:id')
app.get('/camps/new', (req, res) => {
    const camp = CampgroundPrototype;

    res.render('camps/new', { camp });
}); // app.get '/camps/new'

// this route must be BEFORE the route app.get('/camps/:id')
app.post('/camps/new', async (req, res) => {
    const camp = req.body;

    const joiValidateResult = CampgroundValidator.validate(camp);
    if (joiValidateResult.error) {
        camp.error = joiValidateResult.error;
        console.log(`joi/> ${camp.error}`.red);

        res.status(500);
        res.render('camps/new', { camp });

        return;
    } // if

    campInstance = new Campground(req.body);
    await campInstance.save();

    res.redirect(`/camps/${campInstance.id}`);
}); // app.post '/camps/new'
/* camps/new --------------------------------------------------- END */

/* camps/:id --------------------------------------------------- START */
// this route must be AFTER the route app.get('/camps/new')
app.get('/camps/:id', async (req, res) => {
    try {
        const camp = await Campground.findById(req.params.id);
        res.render('camps/view', { camp });
    } // try
    catch(ex) {
        res.status(404);
        res.render('error-documents/404-not-found');
    } // catch
}); // app.get '/camps/id'
/* camps/:id --------------------------------------------------- END */

/* camps/edit --------------------------------------------------- START */
app.get('/camps/:id/edit', async (req, res) => {
    try {
        const camp = await Campground.findById(req.params.id);
        res.render('camps/edit', { camp });
    } // try
    catch(ex) {
        res.status(500);
        res.render('error-documents/500-internal-error');
    } // catch
}); // app.get '/camps/edit'

app.post('/camps/:id/edit', async (req, res) => {
    const { id } = req.params;
    const joiValidateResult = CampgroundValidator.validate(req.body);

    if (joiValidateResult.error) {
        const camp = { ...req.body, id: id, error: joiValidateResult.error };

        console.log(`joi/> ${joiValidateResult.error}`.red);

        res.status(500);
        res.render('camps/edit', { camp });
        
        return;
    } // if
    
    try {
        await Campground.findByIdAndUpdate(id, req.body);
        res.redirect(`/camps/${id}/edit`);
    } // try
    catch(ex) {
        res.status(500);
        res.render('error-documents/500-internal-error');
    } // catch
}); // app.post '/camps/edit'

/* camps/delete-------------------------------------------------- START */
app.get('/camps/:id/delete', async (req, res) => {
    try {
        await Campground.findByIdAndDelete(req.params.id);
        res.redirect('/camps');
    } // try
    catch(ex) {
        res.status(500);
        res.render('error-documents/500-internal-error');
    } // catch
}); // app.get '/camps/delete'
/* camps/delete-------------------------------------------------- END */

// another way to implement middleware
app.get('/another-protected-site', verifyPassword, (req, res, next) => {
    res.send('Just a implementation of middleware.');
}); // app.get

app.get('/error', (req, res) => {
    chicken.bark(); // chicken is not defined
});

// error-handling ---------------------------------------------- START
// 404
app.use( (req, res) => {
    res.status(404);
    res.render('error-documents/404-not-found');
}); // app.use

// 500
app.use( (err, req, res, next) => {
    console.log();
    console.log('*****************************************************************************'.red);
    console.log('******************************** ERROR / START ******************************'.red);
    console.log(err);
    console.log('******************************** ERROR / END ********************************'.red);
    console.log('*****************************************************************************'.red);
    console.log();

    res.status(500);
    res.render('error-documents/500-internal-error');
}); // app.use
// error-handling ---------------------------------------------- END
// ROUTING ----------------------------------------------------------------------------------------------- END

// mongo ------------------------------------------------------------------------------------------------- START
// connect to mongo
mongoose.connect(CONST_CONNECTION_STRING, CONST_CONNECTION_OPTIONS)
    .then( () => {
        console.log( colorman.magenta(`mongodb/> Connection to '${CONST_CONNECTION_STRING}' established.`) );
    }) // then
    .catch( (err) => {
        console.log("mongodb/> Error occurred.".red);
        console.log(err);
    }); // catch
// mongo ------------------------------------------------------------------------------------------------- END